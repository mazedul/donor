@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Donor's Profile</div>

                <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">Name: {{ $user->name }}</li>
                    <li class="list-group-item">Email: <a href="mailto:{{ $user->email }}">{{ $user->email }}</a></li>
                    <li class="list-group-item">Blood Group: <span class="badge badge-primary badge-pill">{{ $user->blood }}</span></li>
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
